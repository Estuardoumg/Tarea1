package clases.umg.vehiculos.principal;

import java.util.InputMismatchException;
import java.util.Scanner;

import clases.umg.vehiculos.clases.Propietario;
import clases.umg.vehiculos.clases.Vehiculo;

/**
 * Created by PC on 07/07/2017.
 */
public class ConsolaApp {

    public static void main(String[] args) {
        int opcion;
        int numeroVehiculo = 0;
        Scanner sc = new Scanner(System.in);
        boolean salir = false;
        Propietario propietario = null;
        while(!salir){
            System.out.println("Menu Principal");
            System.out.println("1. ingresar propietario");
            System.out.println("2. Imprimir datos del propietario");
            System.out.println("3. Agregar Vehiculo");
            System.out.println("4. Listar vehiculos");
            System.out.println("6. salir");
            try{
                System.out.println("Seleccione Una Opcion");
                opcion=sc.nextInt();

               switch(opcion) {
                   case 1:

                       System.out.println("Ingresa la cantidad de vehiculos para el propietario ");
                       Integer cantidadVehiculo = sc.nextInt();
                       propietario = new Propietario(cantidadVehiculo);
                       System.out.println("Ingrese el codigo del propietario: ");
                       propietario.setCodigo(sc.nextInt());
                       System.out.println("Ingrese el nombre del propietario: ");
                       propietario.setNombre(sc.next());
                       System.out.println("Ingrese el edad del propietario: ");
                       propietario.setEdad(sc.nextInt());
                       System.out.println("Ingrese el sexo del propietario M/F: ");
                       propietario.setSexo(sc.next());
                       break;
                   case 2:
                       System.out.println("Datos del Propietario");
                       System.out.println(propietario.toString());
                       break;
                   case 3:
                       Vehiculo vehiculo = new Vehiculo();
                       System.out.println("=================================");
                       System.out.println("Ingrese placa: ");
                       vehiculo.setPlaca(sc.next());
                       System.out.println("Ingrese color: ");
                       vehiculo.setColor(sc.next());
                       System.out.println("Ingrese modelo: ");
                       vehiculo.setModelo(sc.next());
                       System.out.println("Ingrese marca: ");
                       vehiculo.setMarca(sc.next());
                       propietario.agregarVehiculo(vehiculo, numeroVehiculo, propietario.getVehiculos().length);
                       numeroVehiculo++;
                       break;
                       case 4:
                           System.out.println("Vehiculos");
                           for (int x = 0 ; x < propietario.getVehiculos().length ; x++){
                               System.out.println(propietario.getVehiculos()[x]);
                           }
                   break;
                   case 6:
                       salir = true;
                       break;
                   default:
                       System.out.println("Debe seleccionar una opcion entre 1 y 6");


               }

            }catch(InputMismatchException e){
                System.out.println("Debe Ingresar un numero");
                        sc.next();
            }

        }
    }
}
